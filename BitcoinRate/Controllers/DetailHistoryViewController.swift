//
//  DetailHistoryViewController.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

var statusText = ""

class DetailHistoryViewController: BackgroundAlertViewController{
    
    // dependency
    var data: Transactions!
    
    let containerView = UIView()
    
    let aboutLabel = UILabel(text: "Detail Info", font: .boldSystemFont(ofSize: 16))
    
    let dismissButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "cross")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.tintColor = .black
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return button
    }()
    
    //top
    let statusName = UILabel(text: "Status")
    let transactionIdName = UILabel(text: "ID")
    let btcPriceName = UILabel(text: "BTC Price")
    let btcAmountName = UILabel(text: "BTC Amount")
    let dateName = UILabel(text: "Date")
    
    //set data
    let statusLabel = UILabel(text: "Status")
    let transactionIdLabel = UILabel(text: "ID")
    let btcPriceLabel = UILabel(text: "BTC Price")
    let btcAmountLabel = UILabel(text: "BTC Amount")
    let dateLabel = UILabel(text: "Date")
    
    
    let separatorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0.3, alpha: 0.3)
        v.constrainHeight(constant: 0.5)
        return v
    }()
    
    // dependency injection constructer
    init(data: Transactions) {
        self.data = data
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tapGR.delegate = self
        
        setupLayouts()
        setupViews()
    }
    
    
    func setupViews() {
        
        if data.type == 1 {
            statusText = "SELL"
            statusLabel.textColor = .green
        } else {
            statusText = "BUY "
            statusLabel.textColor = .red
        }
        
        statusLabel.text = statusText
        transactionIdLabel.text = String(data.tid)
        btcPriceLabel.text = data.price
        btcAmountLabel.text = data.amount
        dateLabel.text = getDateFromTimeStamp(timeStamp: Double(data.date)!)
    }
    
    func setupLayouts() {
        containerView.backgroundColor = .white
        view.addSubview(containerView)
        containerView.centerInSuperview(size: .init(width: view.frame.width - 10, height: 150))
        containerView.layer.cornerRadius = 12.5
        containerView.layer.addShadow()
        
        containerView.addSubview(aboutLabel)
        aboutLabel.anchor(top: containerView.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        aboutLabel.centerXInSuperview()
        
        containerView.addSubview(dismissButton)
        dismissButton.anchor(top: aboutLabel.topAnchor, leading: nil, bottom: nil, trailing: containerView.trailingAnchor, padding: .init(top: -5, left: 0, bottom: 0, right: 10), size: .init(width: 30, height: 30))
        
        
        let topItems = [
            statusName,
            transactionIdName,
            btcPriceName,
            btcAmountName,
            dateName
        ]
        
        let items = [
            statusLabel,
            transactionIdLabel,
            btcPriceLabel,
            btcAmountLabel,
            dateLabel
        ]
        
        [topItems,items].forEach { (a) in
            a.forEach { (i) in
                i.sizeToFit()
                i.textColor = .darkText
                i.font = .systemFont(ofSize: 12)
            }
        }
        
        let topStackView = UIStackView(arrangedSubviews: topItems)
        topStackView.distribution = .equalSpacing
        topStackView.spacing = 5
        topStackView.axis = .horizontal
        topStackView.separator()
        
        let bottomstackView = UIStackView(arrangedSubviews: items)
        bottomstackView.distribution = .fillProportionally
        bottomstackView.spacing = 5
        bottomstackView.axis = .horizontal
        bottomstackView.separator()
        
        let alloverStackView = VerticalStackView(arrangedSubViews: [topStackView, separatorView ,bottomstackView], spacing: 4)
        alloverStackView.distribution = .fillProportionally

        
        containerView.addSubview(alloverStackView)
        alloverStackView.anchor(top: dismissButton.bottomAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: containerView.trailingAnchor, padding: .init(top: 5, left: 10, bottom: 0, right: 0))
        
    
    }
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
          let date = NSDate(timeIntervalSince1970: timeStamp)
          let dayTimePeriodFormatter = DateFormatter()
          dayTimePeriodFormatter.dateFormat = "dd MMM YY, HH:mm"
          dayTimePeriodFormatter.locale = NSLocale.current
          let dateString = dayTimePeriodFormatter.string(from: date as Date)
          return dateString
      }
    
    
    @objc private func handleDismiss() {
        hideAlertScreen(self)
    }
    
   
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let touchView = touch.view, touchView.isDescendant(of: containerView) {
            return false
        }
        return true
    }
}

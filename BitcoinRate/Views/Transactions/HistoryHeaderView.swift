//
//  HistoryHeaderView.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class HistoryHeaderView: UIView {
    
    let statusLabel = UILabel(text: "Status")
    let btcAmountLabel = UILabel(text: "BTC Amount")
    let dateLabel = UILabel(text: "Date")

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .lightText
        
        let items = [
            statusLabel,
            btcAmountLabel,
            dateLabel
        ]
        
        items.forEach { (i) in
            i.sizeToFit()
            i.textColor = .darkText
        }
        
        let stackView = UIStackView(arrangedSubviews: items)
        stackView.distribution = .fillProportionally
        stackView.spacing = 10
        stackView.axis = .horizontal
        
        addSubview(stackView)
        stackView.fillSuperview(padding: .init(top: 0, left: 20, bottom: 0, right: 20))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

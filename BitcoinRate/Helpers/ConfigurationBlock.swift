//
//  ConfigurationBlock.swift
//  BitcoinRate
//
//  Created by Уали on 1/9/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import Foundation

public func Init<Type>(value : Type, @noescape block: (_ object: Type) -> Void) -> Type
{
    block(value)
    return value
}

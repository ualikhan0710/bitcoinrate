//
//  Transactions.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import Foundation

struct Transactions: Decodable {
    let date: String
    let tid: Int
    let price: String
    let type: Int
    let amount: String
}

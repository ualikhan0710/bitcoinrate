//
//  UIViewController+Alert.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

extension UIViewController: UIGestureRecognizerDelegate {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isKind(of: UIButton.self) {
            return false
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        if let topVC = UIApplication.topViewController() {
            topVC.addChild(viewController)
            topVC.view.addSubview(viewController.view)
            viewController.view.frame = topVC.view.bounds
            
            viewController.view.alpha = 0
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                viewController.view.alpha = 1
            })
        }
    }
    
    func hideAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        viewController.willMove(toParent: nil)
        viewController.removeFromParent()
        
        viewController.view.alpha = 1
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            viewController.view.alpha = 0
        }, completion: { (complete: Bool) -> Void in
            viewController.view.removeFromSuperview()
        })
    }
}

extension UIView: UIGestureRecognizerDelegate {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        addGestureRecognizer(tap)
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.isKind(of: UIButton.self) {
            return false
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        endEditing(true)
    }
    
    func showAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        if let topVC = UIApplication.topViewController() {
            topVC.addChild(viewController)
            topVC.view.addSubview(viewController.view)
            viewController.view.frame = topVC.view.bounds
            
            viewController.view.alpha = 0
            
            UIView.animate(withDuration: 0.5, animations: { () -> Void in
                viewController.view.alpha = 1
            })
        }
    }
    
    func hideAlertScreen(_ viewController: UIViewController?) {
        guard let viewController = viewController else { return }
        
        viewController.willMove(toParent: nil)
        viewController.removeFromParent()
        
        viewController.view.alpha = 1
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            viewController.view.alpha = 0
        }, completion: { (complete: Bool) -> Void in
            viewController.view.removeFromSuperview()
        })
    }
}

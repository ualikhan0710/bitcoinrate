//
//  PriceResult.swift
//  BitcoinRate
//
//  Created by Уали on 1/8/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import Foundation

struct PriceResult: Decodable {
    let time: Time
    let bpi: BPI
}

struct Time: Decodable {
    let updated: String
    let updatedISO: String
    let updateduk: String?
}

struct BPI: Decodable {
    let usd: DetailBPI
    let eur: DetailBPI?
    let kzt: DetailBPI?
    
    enum CodingKeys: String, CodingKey {
        case usd = "USD"
        case eur = "EUR"
        case kzt = "KZT"
    }
           
}

struct DetailBPI: Decodable {
    let code: String
    let rate: String
    let description: String
    let rate_float: Float
}

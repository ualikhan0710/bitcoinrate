//
//  BaseTapBarController.swift
//  BitcoinRate
//
//  Created by Уали on 1/7/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class BaseTapBarController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewControllers = [
            createNavController(viewController: MainViewController(), title: "Текущий курс", tabBarTitle: "Главное", imageName: "first_tab_icon"),
            createNavController(viewController: TransactionsViewController(), title: "История транзакции", tabBarTitle: "Транзакции", imageName: "sec_tab_icon"),
            createNavController(viewController: ConverterViewController(), title: "Конвертер", tabBarTitle: "Конвертер", imageName: "third_tab_icon")
        ]
    }
    
    
    fileprivate func createNavController(viewController: UIViewController, title: String, tabBarTitle: String, imageName: String) -> UIViewController {
        let navController = UINavigationController(rootViewController: viewController)
        navController.navigationBar.prefersLargeTitles = false
        viewController.navigationItem.title = title
        navController.tabBarItem.title = tabBarTitle
        navController.tabBarItem.image = UIImage(named: imageName)
        
        return navController
    }
    
}


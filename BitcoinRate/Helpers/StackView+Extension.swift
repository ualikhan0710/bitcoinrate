//
//  StackView+Extension.swift
//  BitcoinRate
//
//  Created by Уали on 1/8/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class VerticalStackView: UIStackView {

    init(arrangedSubViews: [UIView], spacing: CGFloat = 0){
        super.init(frame: .zero)
        
        arrangedSubViews.forEach({addArrangedSubview($0)})
        
        self.spacing = spacing
        self.axis = .vertical
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension UIStackView {
    func insertSeparator(_ createSeparator: (() -> UIView)) {
        let subviews = self.arrangedSubviews
        
        for v in subviews {
            self.removeArrangedSubview(v)
        }
        
        for v in subviews {
            self.addArrangedSubview(v)
            self.addArrangedSubview(createSeparator())
        }
        
    }
    
    func separator() {
        self.insertSeparator { () -> UIView in
            let separator = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 0.5, height: 40)))
            separator.widthAnchor.constraint(equalToConstant: 0.5).isActive = true
            separator.backgroundColor = UIColor(white: 0.3, alpha: 0.3)
            return separator
        }
    }
    
}


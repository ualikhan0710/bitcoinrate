//
//  BackgroundAlertViewController.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class BackgroundAlertViewController: UIViewController {

    let tapGR = UITapGestureRecognizer()
    var dataLamda: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        tapGR.addTarget(self, action: #selector(hideController(_:)))
        view.addGestureRecognizer(tapGR)
    }
    
    @objc func hideController(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: sender.view)
        guard let touchedView = view.hitTest(point, with: nil) else { return }
        if touchedView == view {
            hideAlertScreen(self)
            dataLamda?()
        }
    }
}

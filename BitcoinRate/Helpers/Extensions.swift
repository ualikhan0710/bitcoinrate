//
//  Extensions.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

extension UILabel {
    convenience init(text: String, font: UIFont = .systemFont(ofSize: 14), numberOfLines: Int = 1) {
        self.init(frame: .zero)
        self.text = text
        self.font = font
        self.numberOfLines = numberOfLines
    }
}

//
//  TrancationViewCell.swift
//  BitcoinRate
//
//  Created by Уали on 1/9/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class TrancationViewCell: UICollectionViewCell {
    
    var historyData: Transactions! {
        didSet {
            if historyData.type == 1 {
                statusText = "SELL"
                statusLabel.textColor = .green
            } else {
                statusText = "BUY "
                statusLabel.textColor = .red
            }
            
            statusLabel.text = statusText
            btcAmountLabel.text = historyData.amount
            dateLabel.text = getDateFromTimeStamp(timeStamp: Double(historyData.date)!)
        }
    }
    
    let statusLabel = UILabel(text: "Status")
    let btcAmountLabel = UILabel(text: "BTC Amount")
    let dateLabel = UILabel(text: "Date")
    
    let separatorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0.3, alpha: 0.3)
        return v
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let items = [
            statusLabel,
            btcAmountLabel,
            dateLabel
        ]
        
        items.forEach { (i) in
            i.sizeToFit()
            i.textColor = .darkText
        }
        
        let stackView = UIStackView(arrangedSubviews: items)
        stackView.distribution = .equalSpacing
        stackView.spacing = 5
        stackView.axis = .horizontal
        
        addSubview(stackView)
        stackView.fillSuperview(padding: .init(top: 10, left: 10, bottom: 10, right: 5))
        
        stackView.separator()
        
        addSubview(separatorView)
        separatorView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, size: .init(width: 0, height: 0.5))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        let date = NSDate(timeIntervalSince1970: timeStamp)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, HH:mm"
        dayTimePeriodFormatter.locale = NSLocale.current
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
}

//
//  TransactionsViewController.swift
//  BitcoinRate
//
//  Created by Уали on 1/9/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class TransactionsViewController: BaseListController, UICollectionViewDelegateFlowLayout {
    
    fileprivate let cellId = "cellId"
    fileprivate let headerView = HistoryHeaderView()
    fileprivate var refresher:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .white
        
        collectionView.contentInset = .init(top: 40, left: 0, bottom: 0, right: 0)
        collectionView.register(TrancationViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 2)
        
        view.addSubview(headerView)
        headerView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: .init(width: 0, height: 40))
        
        fetchData()
        
        pullRefresh()
    }
    
    // MARK:- Pull to refresh
    fileprivate func pullRefresh() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.collectionView!.refreshControl = refresher
        self.refresher.tintColor = UIColor.gray
        self.refresher.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    @objc func refreshData() {
        print("refresh")
        
        self.collectionView!.refreshControl?.beginRefreshing()
        self.fetchData()
        
        stopRefresher()
    }
    
    func stopRefresher() {
        self.collectionView!.refreshControl?.endRefreshing()
    }
    var results = [Transactions]()
    
    fileprivate func fetchData() {
        
        Service.shared.fetchTransactions{ (transactions, err) in
            if let err = err {
                print("Failed to fetch transactions:", err)
                return
            }

            self.results = transactions ?? []
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.stopRefresher()
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = results[indexPath.item]
        let historyDetail = DetailHistoryViewController(data: data)
        self.showAlertScreen(historyDetail)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results.count
    }
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TrancationViewCell
        cell.historyData = results[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width, height: 50)
    }
    
}


/*
 
 fileprivate let footerId = "footerId"
 
 
 collectionView.register(HistoryLoadingFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerId)
 collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
 at: .top,
 animated: true)
 
 
     override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
         let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath)
         return footer
     }
 
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
         let height: CGFloat = isDonePaginating ? 0 : 100
         return .init(width: view.frame.width, height: 0)
     }
 
//----------------------------------------------
// pagination
// ----------------------------------------------
     var isPaginating = false
     var isDonePaginating = false
 
 if indexPath.item == results.count - 1 && !isPaginating {
 print("fetch more data")
 
 isPaginating = true
 
 
 let urlString = "https://www.bitstamp.net/api/transactions/\(params)&offset=\(results.count)&limit=20"
 
 Service.shared.fetchGenericJSONData(urlString: urlString) { (searchResult: [Transactions]?, err) in
 
 if let err = err {
 print("Failed to paginate data:", err)
 return
 }
 
 if searchResult?.count == 0 {
 self.isDonePaginating = true
 }
 
 sleep(2)
 
 self.results += searchResult ?? []
 DispatchQueue.main.async {
 self.collectionView.reloadData()
 }
 self.isPaginating = false
 }
 
 
 }
 */

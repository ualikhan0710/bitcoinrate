//
//  ConverterViewController.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class ConverterViewController: BaseListController, UICollectionViewDelegateFlowLayout {

    fileprivate let cellId = "cellId"
    
    fileprivate var refresher: UIRefreshControl!
    
    fileprivate let cell = ConvertorCell()
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        collectionView.register(ConvertorCell.self, forCellWithReuseIdentifier: cellId)
        
        pullRefresh()
    }

    
    @objc fileprivate func handleSumAdded() {
        
        guard var sumText = cell.sumTextField.text else { return }
        
        if sumText == "" {
            sumText = "Введите сумму"
        } else {
            fetchDates()
        }
        
        
    }
    
    // MARK:- Setup Collection View
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 40, height: 70)
    }
    
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ConvertorCell
        
        cell.sumTextField.addTarget(self, action: #selector(handleSumAdded), for: .valueChanged)
        cell.sumTextField.delegate = self
        
        if indexPath.item == 0 {
            cell.imageView.image = UIImage(named: "BTC")
            cell.nameLabel.text = "Bitcoin"
            cell.currencyLabel.text = "₿"
            return cell
        } else if indexPath.item == 1 {
            cell.imageView.image = UIImage(named: "USD")
            cell.nameLabel.text = "United States Dollar"
            cell.currencyLabel.text = "$"
            return cell
        } else if indexPath.item == 2  {
            cell.imageView.image = UIImage(named: "EUR")
            cell.nameLabel.text = "Euro"
            cell.currencyLabel.text = "€"
            return cell
        } else {
            cell.imageView.image = UIImage(named: "KZT")
            cell.nameLabel.text = "Kazakhstan Tenge"
            cell.currencyLabel.text = "₸"
        }
        
        return cell
    }
  
    
    fileprivate let spacing: CGFloat = 16
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 12, left: 20, bottom: 12, right: 20)
    }
 
    
     //MARK:- Pull to refresh
    fileprivate func pullRefresh() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.collectionView!.refreshControl = refresher
        self.refresher.tintColor = UIColor.gray
        self.refresher.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    @objc func refreshData() {
        print("refresh")
        
        self.collectionView!.refreshControl?.beginRefreshing()
        self.fetchDates()
        
        stopRefresher()
    }
    
    func stopRefresher() {
        self.collectionView!.refreshControl?.endRefreshing()
    }
     
    // MARK:- Fetch Price
    @objc fileprivate func fetchDates() {
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        
//        Service.shared.fetchDates { (res, err) in
//
//            if let err = err {
//                print("Failed to fetch price:", err)
//                return
//            }
////            self.eurDetailBpiResults = res!.bpi
//            print(res)
//            dispatchGroup.leave()
//        }
        
        Service.shared.sellUSD(value: cell.sumTextField.text!) { (res, err) in
            if let err = err {
                print("Failed to convert usd to bitcoin:", err)
                return
            }
            print(res)
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            print("finished fetching")
            self.collectionView.reloadData()
            self.stopRefresher()
        }
        
    }
    
}

extension ConverterViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField ==  cell.sumTextField{
            let allowedCharacters = CharacterSet(charactersIn:"0123456789 .")
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 14
    }

    
    
}

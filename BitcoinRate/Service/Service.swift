//
//  Service.swift
//  BitcoinRate
//
//  Created by Уали on 1/8/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import Foundation

class Service {
    
    static let shared = Service()
    
    ///Fetching Price from CoinDesk Bitcoin
    func fetchPrice(completion: @escaping (PriceResult?, Error?)-> () ){
        print("Fetching Price from CoinDesk Bitcoin")
        let urlString = "https://api.coindesk.com/v1/bpi/currentprice.json"
        
        fetchGenericJSONData(urlString: urlString, completion: completion)
        
    }
    
    func convertForTenge(completion: @escaping (PriceResult?, Error?)-> () ){
        print("Convert USD to KZT")
        let urlString = "https://api.coindesk.com/v1/bpi/currentprice/KZT.json"
        
        fetchGenericJSONData(urlString: urlString, completion: completion)
        
    }
    
    func sellUSD(value: String,completion: @escaping (Double?, Error?)-> () ){
        print("sell and buy USD")
        let urlString = "https://blockchain.info/tobtc?currency=USD&value=\(value)"
        
        fetchGenericJSONData(urlString: urlString, completion: completion)
    }
    
    
    func fetchDates(dateUrlString: String,completion: @escaping (DateResult?, Error?)-> () ){
        print("Fetching dates for Graphic")
        
        fetchGenericJSONData(urlString: dateUrlString, completion: completion)
        
    }
    
    func fetchTransactions(completion: @escaping ([Transactions]?, Error?)-> () ){
        print("Fetch Transactions")
        let params = ["time" : "day"]
        let urlString = "https://www.bitstamp.net/api/transactions/"
        sendRequestWithParams(urlString, parameters: params, completion: completion)
        
    }
    
    func sendRequestWithParams<T: Decodable>(_ url: String, parameters: [String: String], completion: @escaping (T?, Error?) -> () ) {
        var components = URLComponents(string: url)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        let request = URLRequest(url: components.url!)

        let task = URLSession.shared.dataTask(with: request) { data, response, err in
            if let err = err {
                completion(nil, err)
                return
            }
            do {
                let objects = try JSONDecoder().decode(T.self, from: data!)
                completion(objects,nil)
            } catch {
                completion(nil, error)
                print("Failed to decode:",error)
            }
        }
        task.resume()
    }
    
    
    func fetchGenericJSONData<T: Decodable>(urlString: String , completion: @escaping (T?, Error?) -> () ){
        
        guard let url = URL(string: urlString) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            
            if let err = err {
                completion(nil, err)
                return
            }
            do {
                let objects = try JSONDecoder().decode(T.self, from: data!)
                completion(objects,nil)
            } catch {
                completion(nil, error)
                print("Failed to decode:",error)
            }
            }.resume()
    }
    
}

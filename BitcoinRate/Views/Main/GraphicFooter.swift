//
//  GraphicFooter.swift
//  BitcoinRate
//
//  Created by Уали on 1/8/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit
import Charts

class GraphicFooter: UICollectionReusableView {
    
    var lineChartView = LineChartView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 330))
    
    
    lazy var datesSegmentedControl: UISegmentedControl = {
        let sc = UISegmentedControl(items: ["Days", "Months", "Years"])
        sc.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 13.0, *) {
            sc.selectedSegmentTintColor = .brown
            
            sc.setTitleTextAttributes([.foregroundColor : UIColor.white] , for: .selected)
            sc.setTitleTextAttributes([.foregroundColor : UIColor.darkGray] , for: .normal)
            
        } else {
            sc.tintColor = .brown
        }
        
        sc.selectedSegmentIndex = 0
        return sc
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(datesSegmentedControl)
        datesSegmentedControl.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: 20), size: .init(width: 0, height: 30))
        addSubview(lineChartView)
        lineChartView.anchor(top: datesSegmentedControl.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        setChartValues()
    }
        
        
    func setChartValues(_ count : Int = 0, _ labelText: String = "Days") {
        let values = (0..<count).map { (i) -> ChartDataEntry in
            let val = Double(arc4random_uniform(UInt32(count)) + 3)
            return ChartDataEntry(x: Double(i), y: val)
        }
        
        let set1 = LineChartDataSet(entries: values, label: labelText)
        let data = LineChartData(dataSet: set1)
        
        self.lineChartView.data = data
    }
   
   
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

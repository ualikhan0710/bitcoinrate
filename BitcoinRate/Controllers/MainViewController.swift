//
//  MainViewController.swift
//  BitcoinRate
//
//  Created by Уали on 1/8/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class MainViewController: BaseListController, UICollectionViewDelegateFlowLayout {

    fileprivate let cellId = "cellId"
    fileprivate let footerId = "footerId"
    
    fileprivate var detailBpiResults: BPI?
    fileprivate var eurDetailBpiResults: BPI?
    fileprivate var dateData: DateResult?
    fileprivate var footer = GraphicFooter()
    
    fileprivate var imageName: String = ""
    fileprivate var currencyLabel: String = ""
    fileprivate var ratingsLabel: String = ""
    fileprivate var nameLabel: String = ""
    
    fileprivate var datesUrlString: String = "https://api.coindesk.com/v1/bpi/historical/close.json?start=2013-09-01&end=2013-09-07"
    fileprivate var labelText: String = "Days"
    
    fileprivate var refresher:UIRefreshControl!
    
    var timer: Timer?
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(CurrencyCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(GraphicFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footerId)
        
        collectionView.contentInset = .init(top: 0, left: 0, bottom: 50, right: 0)
        
        fetchPrice()
        
        pullRefresh()
        
        // MARK:- Timer
//        timer?.invalidate()
//        timer = Timer.scheduledTimer(timeInterval: 120.0, target: self, selector: #selector(fetchPrice), userInfo: nil, repeats: true);
        
    }
    
   
    
    // MARK:- Pull to refresh
    fileprivate func pullRefresh() {
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.collectionView!.refreshControl = refresher
        self.refresher.tintColor = UIColor.gray
        self.refresher.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
    }
    
    @objc func refreshData() {
        print("refresh")
        
        self.collectionView!.refreshControl?.beginRefreshing()
        self.fetchPrice()
        
        stopRefresher()
    }
    
    func stopRefresher() {
        self.collectionView!.refreshControl?.endRefreshing()
    }
    
    // MARK:- Fetch Price
    @objc fileprivate func fetchPrice() {
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        Service.shared.fetchPrice { (res, err) in
            
            if let err = err {
                print("Failed to fetch price:", err)
                return
            }
            self.eurDetailBpiResults = res!.bpi
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        Service.shared.convertForTenge{ (res, err) in
            if let err = err {
                print("Failed to convert usd to kzt:", err)
                return
            }
            
            self.detailBpiResults = res!.bpi

            dispatchGroup.leave()
        }
            
        dispatchGroup.enter()
        Service.shared.fetchDates(dateUrlString: datesUrlString) { (res, err) in
            
            if let err = err {
                print("Failed to fetch dates:", err)
                return
            }
            
            self.dateData = res!
            dispatchGroup.leave()
            
        }
        
        dispatchGroup.notify(queue: .main) {
            print("finished fetching")
            self.footer.setChartValues((self.dateData?.bpi.count)!,self.labelText)
            self.collectionView.reloadData()
            self.stopRefresher()
        }
        
    }
    
    // MARK:- Setup Collection View
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 40, height: 70)
    }
    
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CurrencyCell
        
        let detail = detailBpiResults
        let eurDetail = eurDetailBpiResults
        var val = "₸"
        
        switch indexPath.item {
        case 0:
            imageName = detail?.usd.code ?? "USD"
            nameLabel = detail?.usd.description ?? ""
            ratingsLabel = detail?.usd.rate ?? ""
            currencyLabel = detail?.usd.code ?? ""
            val = "$"
            break
        case 1:
            imageName = eurDetail?.eur?.code ?? "EUR"
            nameLabel = eurDetail?.eur?.description ?? ""
            ratingsLabel = eurDetail?.eur?.rate ?? ""
            currencyLabel = eurDetail?.eur?.code ?? ""
            val = "€"
            break
        default:
            imageName = detail?.kzt?.code ?? "KZT"
            nameLabel = detail?.kzt?.description ?? "Kazakhtan Tenge"
            ratingsLabel = detail?.kzt?.rate ?? ""
            currencyLabel = detail?.kzt?.code ?? ""
            break
        }

        cell.imageView.image = UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal)
        cell.currencyLabel.text = currencyLabel
        cell.ratingsLabel.text = "\(ratingsLabel) \(val)"
        cell.nameLabel.text = nameLabel
        
        return cell
    }
  
    
    fileprivate let spacing: CGFloat = 16
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 12, left: 20, bottom: 12, right: 20)
    }
    
    //MARK:- Footer Items
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath) as! GraphicFooter
        footer.datesSegmentedControl.addTarget(self, action: #selector(handleGraphicChange), for: .valueChanged)
        return footer
    }
    
    @objc func handleGraphicChange(){
          
           switch footer.datesSegmentedControl.selectedSegmentIndex {
              case 0:
                  // days
                  datesUrlString = "https://api.coindesk.com/v1/bpi/historical/close.json?start=2013-09-01&end=2013-09-07"
                  labelText = "Days"
                  break
              case 1:
                  datesUrlString = "https://api.coindesk.com/v1/bpi/historical/close.json"
                  labelText = "Months"
                  break
              default:
                  datesUrlString = "https://api.coindesk.com/v1/bpi/historical/close.json?start=2013-09-01&end=2014-09-01"
                  labelText = "Years"
                  break
              }
           let count = Int(arc4random_uniform(20) + 3)
           footer.setChartValues(count,labelText)
           
          }
    
   
    
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
          return .init(width: view.frame.width, height: 350)
    }
    
}

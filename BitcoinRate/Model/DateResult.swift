//
//  DateResult.swift
//  BitcoinRate
//
//  Created by Уали on 1/10/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import Foundation


struct Record: Decodable {
    let date: String
    let value: Double?
}
struct DateResult: Decodable {
    let bpi: [String: Double]
    let time: Time
}

//
//  CurrencyCell.swift
//  BitcoinRate
//
//  Created by Уали on 1/8/20.
//  Copyright © 2020 Uali. All rights reserved.
//

import UIKit

class CurrencyCell: UICollectionViewCell {
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "USD")?.withRenderingMode(.alwaysOriginal)
        iv.clipsToBounds = true
        iv.constrainWidth(constant: 45)
        iv.constrainHeight(constant: 45)
        return iv
    }()
    
    let currencyLabel: UILabel = {
        let label = UILabel()
        label.text = "USD"
        label.font = UIFont.systemFont(ofSize: 19, weight: .heavy)
        label.textColor = .black
        return label
    }()
    
    let ratingsLabel: UILabel = {
        let label = UILabel()
        label.text = "8000 $"
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.textColor = .black
        return label
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "United States Dollar"
        label.font = UIFont.systemFont(ofSize: 13, weight: .light)
        label.textColor = .darkText
        return label
    }()
    
    let separatorView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0.3, alpha: 0.3)
        return v
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayouts()
    }
    
    //MARK:- Setup Layouts
    fileprivate func setupLayouts() {
        backgroundColor = .white
        
        let stackView = UIStackView(arrangedSubviews: [
            imageView,
            VerticalStackView(arrangedSubViews: [currencyLabel, nameLabel],spacing: 5),
            UIView(),
            ratingsLabel
        ])
        
        stackView.spacing = 12
        stackView.alignment = .center
        
        addSubview(stackView)
        stackView.fillSuperview()
        
        addSubview(separatorView)
        separatorView.anchor(top: nil, leading: currencyLabel.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: -8, right: 0), size: .init(width: 0, height: 0.5))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
